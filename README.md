# Upload and download file
## How to run
### Config .env file
- add host, port, token, folder name
### Run in docker
```sh
docker build -t IMAGE:TAG .
docker run --rm -it -v path-of-env-file:/app/.env IMAGE:TAG
```
## Usage
### Upload file
```sh
curl -x POST -F 'filename=@path-of-file' -F 'token=cp-token' http://URL:PORT/upload
```
### Download file
#### Get files name
```sh
curl http://URL:PORT/getfiles
```
#### Download file
```sh
curl http://URL:PORT/download/filename -o filename
```