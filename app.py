from flask import Flask, render_template, request, send_from_directory
from werkzeug.utils import secure_filename
import os, sys, getopt, subprocess
from dotenv import load_dotenv

load_dotenv()
FILE_FOLDER = os.getenv('FILE_FOLDER')
TOKEN = os.getenv('TOKEN')
HOST = os.getenv('HOST')
PORT = os.getenv('PORT')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = FILE_FOLDER
app.config['DOWNLOAD_FOLDER'] = FILE_FOLDER

if not os.path.isdir(FILE_FOLDER):
    os.mkdir(FILE_FOLDER)

@app.route('/')
def hello():
    return render_template('index.html', title='hello')

@app.route('/upload', methods = ['POST'])
def upload_files():
    if request.method == 'POST':
        if request.form['token'] != TOKEN:
            return "upload failed"
        f = request.files['filename']
        f_name = secure_filename(f.filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
        return "upload successfully"
def run_command():
    cmd = subprocess.run('ls '+app.config['DOWNLOAD_FOLDER'], capture_output=True,shell=True,text=True)
    result = cmd.stdout
    return result

@app.route('/getfiles', methods = ['GET'])
def command_server():
    return run_command()

@app.route('/download/<filename>', methods = ['GET'])
def download_file(filename):
    return send_from_directory(os.path.join(app.config['DOWNLOAD_FOLDER']),filename, as_attachment=True)

# def main(argv):
#     try:
#        opts, args = getopt.getopt(argv,":t",["token"])
#        if not opts:
#           print("python3 upload.py -t <token>>")
#           sys.exit()
#     except getopt.GetoptError as e:
#        print(e)
#        sys.exit()
#     for opt, arg in opts:
#        if opt in ("-t","--token"):
#           TOKEN = arg

#     app.run(host=HOST,port=int(PORT),debug=True)

if __name__ == '__main__':
    # main(sys.argv[1:])
    app.run(host=HOST,port=int(PORT),debug=True)
