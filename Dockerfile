FROM python:3.7.8-alpine3.11
WORKDIR /app
ADD . .
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3","app.py"]